<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('_partials/head.php'); ?>
	
	
</head>

<body>
	<?php $this->load->view('_partials/navbar.php'); ?>

	<div class="container">
	
	
<h1 class="h3 mt-5 text-gray-800">ISI PENGADUAN DI FORM BERIKUT</h1>
		<form action="" method="post" style="max-width: 600px;" enctype="multipart/form-data">
			
			 <div>			
				<input type="hidden" name="nikdisplay" class="<?= form_error('nik') ? 'invalid' : '' ?>" placeholder="Nomor Nik" disabled value="<?= $this->session->userdata('user_id'); ?>" required maxlength="32"/>
				<input type="hidden" name="nik" value="<?= $this->session->userdata('user_id'); ?>">
				<div class="invalid-feedback"><?= form_error('nik') ?></div>
			</div> 
		
			<div>
				<label for="isi_laporan">Pesan*</label><br>
				<textarea name="isi_laporan" cols="30" class="<?= form_error('isi_laporan') ? 'invalid' : '' ?>" rows="5" placeholder="write your message" required><?= set_value('isi_laporan') ?></textarea>
				<div class="invalid-feedback"><?= form_error('isi_laporan') ?></div>
			</div>
			
			<div>
			
					<label for="foto">Upload foto anda dan foto selfie KTP</label>
					<input type="file" name="foto" >
					<div class="invalid-feedback"><?= form_error('foto') ?></div>
				
			
			</div>

			

			<div style="display: flex; gap: 1rem">
				<input type="submit" class="button button-primary" value="Kirim">
				<input type="reset" class="button" value="Reset">
			</div>
		</form>
	</div>
	<?php $this->load->view('_partials/footer.php'); ?>
</body>

</html>
