<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view('_partials/head.php'); ?>
</head>
<body>
    <?php $this->load->view('_partials/navbar.php'); ?>

    <form action="<?= base_url('registrasi_a/add')?>" method="post" style="max-width: 600px; margin: auto; width: 800px;">
        <label for="id_petugas">
            Masukkan Id Petugas
        </label>
        <input type="text" name="id_petugas" placeholder="ID PETUGAS">
        <label for="nama">
            Masukkan Nama
        </label>
        <input type="text" name="nama_petugas" placeholder="Nama">
        <label for="username">
            Masukkan Username
        </label>
        <input type="text" name="username" placeholder="Username">
        <label for="password">
            Masukkan Password
        </label>
        <input type="password" name="password" placeholder="Password">
        <label for="telp">
            Masukkan Nomor Telp
            <input type="text" name="telp" placeholder="No. telp">
        </label>
        <!-- <div>
			<label for="role">Registrasi Sebagai:</label>

			<select name="role" id="role">
			<option value="masyarakat">Masyarakat</option>
			<option value="petugas">Admin</option>			
			</select>
			</div> -->
        <div style="display: flex; gap: 1rem">
				<input type="submit" class="button button-primary" value="Kirim">
				<input type="reset" class="button" value="Reset">
			</div>
    </form>
</body>
</html>