<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php') ?>
</head>

<body>
	<main class="main">
		<?php $this->load->view('admin/_partials/side_nav.php') ?>

		<div class="content">
			<h1>Update Profile</h1>

			<form action="" method="POST">
				<div>
					<label for="nama_petugas">Nama</label>
					<input type="text" name="nama_petugas" class="<?= form_error('nama_petugas') ? 'invalid' : '' ?>"
					value="<?= form_error('nama_petugas') ? set_value('nama_petugas') : $current_user->nama_petugas ?>" 
					required maxlength="32"/>
					<div class="invalid-feedback">
						<?= form_error('nama_petugas') ?>
					</div>
				</div>
				 <div>
					<label for="telp">Nomor Telfon</label>
					<input type="number" name="telp" class="<?= form_error('telp') ? 'invalid' : '' ?>"
					value="<?= form_error('telp') ? set_value('telp') : $current_user->telp ?>" 
					required maxlength="32"/>
					<div class="invalid-feedback">
						<?= form_error('telp') ?>
					</div>
				</div> 

				<div>
					<button type="submit" name="save" class="button button-primary">Save Update</button>
				</div>
			</form>

			<?php $this->load->view('admin/_partials/footer.php') ?>
		</div>
	</main>
</body>

</html>
