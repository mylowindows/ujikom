<aside class="side-nav">
	<div class="brand">
		<h2>Ujikom</h2>
		<div style="display: flex; gap: 0.8rem; margin:2rem 0;">
			
			<div>
				<h5>Nama Petugas :</h5>
				<b><?= htmlentities($current_user->nama_petugas) ?></b>
				
			</div>
		</div>
	</div>
	<nav>
		<a href="<?= site_url('admin/dashboard') ?>">Overview</a>
		<!-- <a href="<?= site_url('admin/post') ?>">Post</a> -->
		<a href="<?= site_url('admin/pengaduan') ?>">List Pengaduan</a>
		<a href="<?= site_url('auth/logout') ?>">Logout</a>
	</nav>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</aside>
