<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view('admin/_partials/head.php') ?>
</head>

<body>
	<main class="main">
		<?php $this->load->view('admin/_partials/side_nav.php') ?>

		<div class="content">
			<h1>List Pengaduan</h1>
			<!-- <?php var_dump($feedbacks); ?> -->
			

			<?php foreach($pengaduans as $pengaduan): ?>
			<div class="card">
				<div class="card-header">
					<div>
						<b><?= $pengaduan->nik ?></b> </small>
						<br> <img src="<?= base_url('upload/pengaduan/') . $pengaduan->foto ;  ?>" alt="..." width="300" height="200">
					</div>
					<div><small class="text-gray"><?= $pengaduan->tgl_pengaduan ?></small></div>
				</div>
				<p><?= $pengaduan->isi_laporan ?></p>
					
                       
					<form action="<?= base_url('tanggapan/create'); ?>" method="POST" >	
						<div class="form-group">
							<p>Status</p>
								<input class="radiostyle" type="radio" name="status" id="status" value="proses" <?php if ($pengaduan->status=='proses') {
							
									echo 'checked'; 
								}?>>
								<label for="age1">Proses</label><br>
								<input class="radiostyle" type="radio" name="status" id="status" value="selesai"  <?php if ($pengaduan->status=='selesai') {
							
								echo 'checked'; 
								}?>>
								<label for="age2">Selesai</label><br>	
							</div>		

					

						<div class="form-group">

							<label for=""></label>
							<textarea type="textarea" class="form-control kolomreply" name="tanggapan" id="" aria-describedby="helpId" placeholder=""></textarea>
							<small id="helpId" class="form-text text-muted"></small>

							<input type="hidden" name="isi_laporan" value="<?= $pengaduan->isi_laporan ?>">
							<input type="hidden" name="id_pengaduan" value="<?= $pengaduan->id_pengaduan ?>">
									
							<button type="submit"	

									class="button button-danger button-small" 
									role="button"
									text=>Submit
							</button>
							<a href="#" 

									data-delete-url="<?= site_url('admin/pengaduan/delete/'.$pengaduan->id_pengaduan) ?>" 
									class="button button-danger button-small" 
									role="button"
									onclick="deleteConfirm(this)">Delete
							</a>
						</div>
					</form>
				
					
			</div>
			
			<?php endforeach ?>

			<?php $this->load->view('admin/_partials/footer.php') ?>
		</div>
	</main>

	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script>
		function deleteConfirm(event){
			Swal.fire({
				title: 'Delete Confirmation!',
				text: 'Are you sure to delete the item?',
				icon: 'warning',
				showCancelButton: true,
				cancelButtonText: 'No',
				confirmButtonText: 'Yes Delete',
				confirmButtonColor: 'red'
			}).then(dialog => {
				if(dialog.isConfirmed){
					window.location.assign(event.dataset.deleteUrl);
				}
			});
		}
	</script>

	<?php if($this->session->flashdata('message')): ?>
		<script>
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000,
				timerProgressBar: true,
				didOpen: (toast) => {
					toast.addEventListener('mouseenter', Swal.stopTimer)
					toast.addEventListener('mouseleave', Swal.resumeTimer)
				}
			})

			Toast.fire({
				icon: 'success',
				title: '<?= $this->session->flashdata('message') ?>'
			})
		</script>
	<?php endif ?>
</body>

</html>
