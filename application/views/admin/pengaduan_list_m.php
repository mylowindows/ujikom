<!DOCTYPE html>
<html lang="en">

<head>
		<?php $this->load->view('_partials/head.php'); ?>
		
</head>

<body>
	<main class="main">
		<?php $this->load->view('_partials/navbar.php') ?>

		<div class="content">
			<h1 class="h1 mt-3 text-gray-800 ">List Pengaduan</h1>
			<!-- <?php var_dump($feedbacks); ?> -->
			

			<?php foreach($pengaduans as $pengaduan): ?>
				<div class="card m-5" style="width: 18rem;">
					<img class="card-img-top" src="<?= base_url('upload/pengaduan/') . $pengaduan->foto ;  ?>" alt="Card image cap">
						<div class="card-body">
							<h5 class="card-title"><?= $pengaduan->tgl_pengaduan ?> <span class="badge bg-success"><?= $pengaduan->status?></span> </h5> 
							<p class="card-text"><?= $pengaduan->isi_laporan ?></p>
						</div>
							<ul class="list-group list-group-flush">						
									<?php foreach($tanggapans as $tanggapan): ?>
									<?php 
										if ($pengaduan->id_pengaduan == $tanggapan->id_pengaduan) { ?>
											<li class="list-group-item"><?= $tanggapan->tanggapan ?></li>

									<?php }
									?>
								
									<?php endforeach ?>
							</ul>
						
				</div>
									
			<?php endforeach ?>

			<?php $this->load->view('_partials/footer.php') ?>
		</div>
	</main>



</body>

</html>
