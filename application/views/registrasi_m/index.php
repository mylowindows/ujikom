<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('_partials/head.php'); ?>
</head>
<body>
    <?php $this->load->view('_partials/navbar.php'); ?>

               
    
    <form   action="<?= base_url('registrasi_m/add')?>" method="post" style="max-width: 600px; margin: auto; width: 800px;">
        <label for="nik">
            Masukkan NIK
        </label>
        <input type="text" name="nik" placeholder="Nomor Nik">
        <label for="nama">
            Masukkan Nama
        </label>
        <input type="text" name="nama" placeholder="Nama">
        <label for="username">
            Masukkan Username
        </label>
        <input type="text" name="username" placeholder="Username">
        <label for="password">
            Masukkan Password
        </label>
        <input type="password" name="password" placeholder="Password">
        <label for="telp">
            Masukkan Nomor Telp
            <input type="text" name="telp" placeholder="No. telp">
        </label>

        <div>
			<label for="foto_ktp">Upload foto anda dan foto selfie KTP</label>
			<input type="file" name="foto_ktp" >
			<div class="invalid-feedback"></div>				                
		</div> 
        
        <div>
				<input type="submit" class="button button-primary" value="Kirim">
				<input type="reset" class="button" value="Reset">
		</div>
    </form>
</body>
</html>