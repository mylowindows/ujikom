<?php

class Profile_model extends CI_Model
{
	private $_table = "user";

	public function profile_rules()
	{
		return [
			[
				'field' => 'nama',
				'label' => 'Nama',
				'rules' => 'required|max_length[32]'
			],
			[
				'field' => 'id_petugas',
				'label' => 'id petugas',
				'rules' => 'required|max_length[32]'
			],
		];
	}

	public function password_rules()
	{
		return [
			[
				'field' => 'password',
				'label' => 'New Password',
				'rules' => 'required'
			],
			[
				'field' => 'password_confirm',
				'label' => 'Password Confirmation',
				'rules' => 'required|matches[password]'
			],
		];
	}

	public function update($data)
	{
		if (!$data['nik']) {
			return;
		}
		return $this->db->update($this->_table, $data, ['nik' => $data['nik']]);
	}
}
