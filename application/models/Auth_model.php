<?php

class Auth_model extends CI_Model
{
	private $_table = "user";

	public function rules()
	{
		return [
			[
				'field' => 'username',
				'label' => 'Username or Email',
				'rules' => 'required'
			],
			[
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|max_length[255]'
			]
		];
	}

	public function login($username, $password, $role)
	{
		if ($role == 'petugas') {
			$tabel = 'petugas';
			$this->db->where('username', $username);
		} else if ($role == 'masyarakat') {
			$tabel = 'masyarakat';
			$this->db->where('nik', $username)->or_where('username', $username);
		} else {
			$tabel = $role;
		}
		
		$query = $this->db->get($tabel);
		$user = $query->row();

		// cek apakah user sudah terdaftar?
		if (!$user) {
			return FALSE;
		}

		// cek apakah passwordnya benar?
		if (!password_verify($password, $user->password)) {
			return FALSE;
		}

		// bikin session
		if($tabel == 'masyarakat'){
			$this->session->set_userdata(['user_id' => $user->nik]);
		}else{
			$this->session->set_userdata(['user_id' => $user->id_petugas]);
		}
		
		
		$newdata = array(
				'username'  => $user->username,				
				'role'      => $role,
				'logged_in' => TRUE
		);
		
		$this->session->set_userdata($newdata);

		if ($role == 'masyarakat') {
			redirect('');
		}
		else {
			redirect('admin/dashboard');
		}
		

		return $this->session->has_userdata('user_id');
	}

	public function current_user()
	{
		if (!$this->session->has_userdata('user_id')) {
			return null;
		}

		$user_id = $this->session->userdata('user_id');
		$tabel = $this->session->userdata('role');
		if ($tabel == "petugas") {
			$query = $this->db->get_where($tabel, ['id_petugas' => $user_id]);
		}else{	
			$query = $this->db->get_where($tabel, ['nik' => $user_id]);
		}
		return $query->row();
	}

	
	private function _update_last_login($id)
	{
		$data = [
			'last_login' => date("Y-m-d H:i:s"),
		];
		$tabel = $this->session->userdata('role');
		if ($tabel == "petugas") {
			return $this->db->update($tabel, $data, ['id_petugas' => $id]);
		}else{	
			return $this->db->update($tabel, $data, ['nik' => $id]);
		}
	}
}
