<?php

class Registrasi_a_model extends CI_Model
{
	private $_table = "petugas";

	public function register_rules()
	{
		return [
			[
				'field' => 'id_petugas',
				'label' => 'id petugas',
				'rules' => 'required|max_length[32]'
			],
			[
				'field' => 'nama_petugas',
				'label' => 'Nama petugas',
				'rules' => 'required|max_length[32]'
			],
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|max_length[32]'
			],
			[
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|max_length[64]'
			],
			[
				'field' => 'telp',
				'label' => 'Telp',
				'rules' => 'required|max_length[32]'
			],
		];
	}

	public function insert()
	{
		$data = array(
			'id_petugas' 		=> $this->input->post('id_petugas'),
			'nama_petugas'		=> $this->input->post('nama_petugas'),
			'username' 	=> $this->input->post('username'),
			'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			'telp' 		=> $this->input->post('telp')
		);

		return $this->db->insert($this->_table, $data);
		
	}
}
