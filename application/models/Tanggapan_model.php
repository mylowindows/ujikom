<?php

class Tanggapan_model extends CI_Model
{

	private $_table = 'tanggapan';

	public function rules()
	{
		return [
			[
				'field' => 'tanggapan',
				'label' => 'tanggapan',
				'rules' => 'required|max_length[500]'
			],
			
		];
	}

	public function get()
	{
		$query = $this->db->get($this->_table);
		return $query->result();
	}

	public function count()
	{
		return $this->db->count_all($this->_table);
	}
	

	public function find_id_pengaduan($id)
	{
		if (!$id) {
			return;
		}

		$query = $this->db->get_where($this->_table, array('id_pengaduan' => $id));
		return $query->row();
	}

	public function find_id_tanggapan($id)
	{
		if (!$id) {
			return;
		}

		$query = $this->db->get_where($this->_table, array('id_tanggapan' => $id));
		return $query->row();
	}

	public function find_id_petugas($id)
	{
		if (!$id) {
			return;
		}

		$query = $this->db->get_where($this->_table, array('id_petugas' => $id));
		return $query->row();
	}


	public function insert($article)
	{
		return $this->db->insert($this->_table, $article);
	}



}
