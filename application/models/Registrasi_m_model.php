<?php

class Registrasi_m_model extends CI_Model
{
	private $_table = "masyarakat";

	public function register_rules()
	{
		return [
			[
				'field' => 'nik',
				'label' => 'Nik',
				'rules' => 'required|max_length[32]'
			],
			[
				'field' => 'nama',
				'label' => 'Nama',
				'rules' => 'required|max_length[32]'
			],
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|max_length[32]'
			],
			[
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|max_length[64]'
			],
			[
				'field' => 'telp',
				'label' => 'Telp',
				'rules' => 'required|max_length[32]'
			],
			[
				'field' => 'foto_ktp',
				'label' => 'Foto KTP',
				'rules' => 'required|max_length[32]'
			],
		];
	}

	public function insert()
	{
		$data = array(
			'nik' 		=> $this->input->post('nik'),
			'nama'		=> $this->input->post('nama'),
			'username' 	=> $this->input->post('username'),
			'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			'telp' 		=> $this->input->post('telp'),
			'foto_ktp' => $this->input->post('foto_ktp')
		);

		return $this->db->insert($this->_table, $data);
		
	}
}
