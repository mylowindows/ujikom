<?php

class Pengaduan_model extends CI_Model
{
	private $_table = "pengaduan";

	public function rules()
	{
		return [
			[
				'field' => 'nik', 
				'label' => 'nik', 
				'rules' => 'required'
			],
		
			
		
			[
				'field' => 'isi_laporan', 
				'label' => 'isi_laporan', 
				'rules' => 'required'
			],
		];
	}

	public function insert($pengaduan)
	{
		if (!$pengaduan) {
			return;
		}

		return $this->db->insert($this->_table, $pengaduan);
	}

	public function get()
	{
		$query = $this->db->get($this->_table);
		return $query->result();
	}

	public function get_nik($nik)
	{
		$query = $this->db->get_where('pengaduan', array('nik' => $nik) );
		return $query->result();
	}

	public function count()
	{
		return $this->db->count_all($this->_table);
	}

	public function delete($id_pengaduan)
	{
		if (!$id_pengaduan) {
			return;
		}

		$this->db->delete($this->_table, ['id_pengaduan' => $id_pengaduan]);
	}
	
	public function update($pengaduan, $data)
	{
		$this->db->where('id_pengaduan', $pengaduan);
		$this->db->update('pengaduan', $data);
	}
	
}
