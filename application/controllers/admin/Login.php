<?php

class Login extends CI_Controller
{
	public function index()
	{
		$this->logins();
	}

	public function logins()
	{
		$this->load->model('auth_model');
		$this->load->library('form_validation');
		

		$rules = $this->auth_model->rules();
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == FALSE){
			return $this->load->view('login_form_admin');
		}

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$role     = $this->input->post('role'); 

		if($this->auth_model->login($username, $password, $role)){

			
		
			redirect('');
		} 
			else {
				$this->session->set_flashdata('message_login_error', 'Login Gagal, pastikan username dan passwrod benar!');
		}

		
		

		$this->load->view('login_form_admin');

	}
	
	

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(site_url());
	}
}
