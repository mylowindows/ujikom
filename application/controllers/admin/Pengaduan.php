<?php

class Pengaduan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('auth_model');
		if(!$this->auth_model->current_user()){
			redirect('auth/login');
		}
	}

	public function index()
	{ 	
		$this->load->model('pengaduan_model');
		$data['pengaduans'] = $this->pengaduan_model->get();
		$data['current_user'] = $this->auth_model->current_user();
		if(count($data['pengaduans']) <= 0){
			$this->load->view('admin/pengaduan_empty', $data);
		} else {
			$this->load->view('admin/pengaduan_list', $data);
		}
	}

	public function delete($id_pengaduan = null)
	{
		if(!$id_pengaduan){
			show_404();
		}

		$this->load->model('pengaduan_model');
		$this->pengaduan_model->delete($id_pengaduan);

		$this->session->set_flashdata('message', 'Data was deleted');
		redirect(site_url('admin/pengaduan'));
	}
}
