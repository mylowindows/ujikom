<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends CI_Controller
{
	public function index()
	{
		$data['meta'] = [
			'title' => 'Pengaduan',
		];

		$this->load->view('home', $data);
	}

	public function about()
	{
		$data['meta'] = [
			'title' => 'Tentang Pengaduan',
		];

		$this->load->view('about', $data);
	}

	public function contact()
	{
		$data['meta'] = [
			'title' => 'Kontak kami!',
		];
		$this->load->library('form_validation');
		
		if($this->input->method() === 'post'){
			$this->load->model('pengaduan_model');
			
			$rules = $this->pengaduan_model->rules();
			$this->form_validation->set_rules($rules);
			
			if ($this->form_validation->run() == FALSE)
			{
				return $this->load->view('contact', $data);
			}

			//mulai upload 
			$config['upload_path']=  './upload/pengaduan/';
			$config['allowed_types']        = 'gif|GIF|jpg|png|jpeg|JPG|PNG|jfif';
			$config['file_name']= uniqid();

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('foto')) {
			
			// echo $config['upload_path'];
			
			 var_dump($this->upload->display_errors());
			// return $this->load->view('contact', $data);

		} else {			

			$pengaduan = [	
				'tgl_pengaduan' => date("Y/m/d"),		
				'nik' => $this->input->post('nik'),	
				'foto' => 	$config['file_name'] . $this->upload->data('file_ext'),
				'isi_laporan' => $this->input->post('isi_laporan')
			];
			
			$pengaduan_saved = $this->pengaduan_model->insert($pengaduan);
			
			if($pengaduan_saved){
				return $this->load->view('contact_thanks');
			}
		}


			
		}

		$this->load->view('contact', $data);
	}
}
