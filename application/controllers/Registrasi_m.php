<?php

class Registrasi_m extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		$this->load->model('Registrasi_m_model');
	}

	public function index()
	{
        $this->load->view('Registrasi_m/index');
    }

	public function add ()
    {
        // echo 'berhasil!';
        $data['query'] = $this->Registrasi_m_model->insert();
        redirect(base_url(''), 'refresh');
    }
    
}