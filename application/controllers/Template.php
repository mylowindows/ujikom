<?php

class Template extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		$this->load->model('Template_model');
	}

	public function index()
	{
        $this->load->view('template/index');
    }

	public function add ()
    {
        // echo 'berhasil!';
        $data['query'] = $this->Template_model->insert();
        redirect(base_url(''), 'refresh');
    }
}