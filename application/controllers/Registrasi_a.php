<?php

class Registrasi_a extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		$this->load->model('Registrasi_a_model');
	}

	public function index()
	{
        $this->load->view('Registrasi_a/index');
    }

	public function add ()
    {      
        $data['query'] = $this->Registrasi_a_model->insert();
        redirect(base_url(''), 'refresh');
    }
    
}