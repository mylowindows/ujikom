<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tanggapan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();		
		$this->load->library('form_validation');
		$this->load->model('Pengaduan_model');
		$this->load->model('Tanggapan_model');
	}

	public function create()
	{
		 echo $this->input->post('status');
		 echo '<br>';
		 echo $this->input->post('tanggapan');
		 echo '<br>';
		 echo $this->input->post('isi_laporan');
		 echo $this->input->post('id_pengaduan');
		 echo $this->input->post('id_petugas');

		 $tanggapan = [	
			
			'id_pengaduan' => $this->input->post('id_pengaduan'),
			'tgl_tanggapan' => date("Y/m/d"),		
			'tanggapan' => $this->input->post('tanggapan'),
			'id_petugas' => $this->session->userdata('user_id')
		];
		
		$tanggapan_saved = $this->Tanggapan_model->insert($tanggapan);

		$data = [
			'status' => $this->input->post('status')
		];

		$pengaduan_saved = $this->Pengaduan_model->update($this->input->post('id_pengaduan'), $data);

		
		redirect('admin/pengaduan');
	}
}
	
	


