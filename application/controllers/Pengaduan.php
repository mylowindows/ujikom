<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengaduan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();		
		$this->load->library('form_validation');
		$this->load->model('Pengaduan_model');
		$this->load->model('Tanggapan_model');
	}

	public function create()
	{
		$data['meta'] = [
			'title' => 'Pengaduan',
		];
		$this->load->library('form_validation');
		
		if($this->input->method() === 'post'){
			$this->load->model('feedback_model');
			
			$rules = $this->feedback_model->rules();
			$this->form_validation->set_rules($rules);
			
			if ($this->form_validation->run() == FALSE)
			{
				return $this->load->view('contact', $data);
			}

			$feedback = [
				'id' => uniqid('', true),
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'message' => $this->input->post('message')
			];
			
			$feedback_saved = $this->feedback_model->insert($feedback);
			
			if($feedback_saved){
				return $this->load->view('contact_thanks');
			}
		}

		$this->load->view('Pengaduan/buat_pengaduan', $data);
	}
	
	public function read()
	{
		$data['pengaduans'] = $this->Pengaduan_model->get_nik($this->session->userdata('user_id'));
		$data['tanggapans'] = $this->Tanggapan_model->get();

		if(count($data['pengaduans']) > 0){
			$this->load->view('admin/pengaduan_list_m.php', $data);
		} else {
			$this->load->view('articles/empty_article.php');
		}
	}
	public function update()
	{
		
	}
	public function delete()
	{
		
	}

}
